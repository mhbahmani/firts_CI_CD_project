FROM python:latest

WORKDIR /app

ENV FLASK_APP main
ENV FLASK_ENV product
ENV PIP_NO_CACHE_DIR 1

ADD ./requirements.txt ./
RUN pip install -r ./requirements.txt

ADD ./ ./

ENTRYPOINT [ "flask" , "run" , "--host=0.0.0.0" ]